/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectliburan;

import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author Icaa
 */
public class possibleNumber {
     public static void main(String[] args) throws IOException {
        Scanner input = new Scanner(System.in);
        System.out.print("Masukan String :");
        String k = input.nextLine();
        int p = k.length();
        int n[] = new int[p];

        //mencari nilai terbesar 2 digit
        int max = n[0];
        int d = 0;
        for (int i = 0; i < k.length(); i++) {
            try {
                String a = k.substring(i, i + 2);
                n[i] = Integer.parseInt(a);
            } catch (Exception e) {
                continue;
            }
            if (max <= n[i]) {
                max = n[i];
                d = i;
            }
        }

        //Kondisi jika nilai terbesar 1 digit
        if (max == 0) {
            for (int i = 0; i < k.length(); i++) {
                try {
                    String a = k.substring(i, i + 1);
                    n[i] = Integer.parseInt(a);
                } catch (Exception e) {
                    continue;
                }
                if (max <= n[i]) {
                    max = n[i];
                    d = i;
                }
            }
        }

        //mencari nilai Terkecil
        int min = 0;
        min = max;
        for (int i = 0; i < k.length(); i++) {
            try {
                String a = k.substring(i, i + 1);
                n[i] = Integer.parseInt(a);
            } catch (Exception e) {
                continue;
            }
            if ((i != d + 1) && (i != d)) {
                if (min >= n[i]) {
                    min = n[i];
                }
            }
        }
        System.out.println(k);
        System.out.println("Nilai Max :" + max);
        System.out.println("Nilai Min :" + min);
    }
}

