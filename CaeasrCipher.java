/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectliburan;

/**
 *
 * @author Alwan
 */
public class CaeasrCipher {

    public static void main(String[] args) {
        {

            int key = 6;
            String[] huruf = new String[26];

            //  isi array huruf
            for (char bchar = 'Z'; bchar >= 'A'; bchar--) {
                huruf[bchar - 65] = String.valueOf(bchar);
            }
            String c = "JGYVXU RGHUXGZUXE";
           
            String t = "";

            //  Proses Dekrip
            ulang:
            for (int a = 0; a < c.length(); a++) {

              
                int ic = -1;
                for (int b = 0; b < huruf.length; b++) {
                    //  Cari nomor tiap karakter cipher dengan kondisi
                    if (String.valueOf(c.charAt(a)).equals(huruf[b])) {//jika huruf ciper sama dengan huruf array
                        ic = b;//menyimpan index huruf ciper jika sudah ditemukan
                    } else {
                        ic = -1;//jika tidak sama dengan huruf ciper maka ic di isi dengan -1
                    }
                     
                    if (ic != -1) {//jika ic tidak sama dengan -1 maka akan di cari huruf sebenarnya dengan menggunakan key
                           ic = ic - key; //ic - key
                        if ((ic) < 0) { //jika ic < 0 maka ic + 26 
                            t += huruf[26 + ic];
                        } else {//jika ic > dari 0 maka ic mod 26 
                            t += huruf[(ic) % 26];
                        }
                        continue ulang;//sintak untuk melanjukan ke pengulangan pertama

                    }
                }
                //jika tidak ditemukan huruf indexnya 
                t += c.charAt(a);

            }
            System.out.println("CiperText       :" + c);

            System.out.println("Hasil Dekrip    :" + t); //  Tampilkan Hasil Dekrip
        }
    }
}
