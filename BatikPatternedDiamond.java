/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectliburan;

import java.util.Scanner;

/**
 *
 * @author Alwan
 */
public class BatikPatternedDiamond {

    public static void main(String[] args) {
        int uk = 0;
        Scanner input = new Scanner(System.in);
        System.out.print("Masukan Ukuran :");
        uk = input.nextInt();//Vaiabel untuk menyimpan nilai ukuran batik diamond

        for (int br = 0; br < uk; br++) {//mambuat baris batik
            for (int kl = 0; kl < uk; kl++) {//membuat kolom batik diamond atas 
                System.out.print("|");
                for (int i = uk; i > 0; i--) {
                    for (int j = 1; j <= kl; j++) {//membuat space sebelum mencetak batik diamond
                        System.out.print(" ");
                    }
                    System.out.print("\\");
                    for (int k = uk - 1; k > kl; k--) {//mencetak batik diamond segitigaterbalik kiri
                        System.out.print("=");
                    }

                    for (int j = uk - 1; j > kl; j--) {//mencetak batik diamon segitigaterbalik kanan 
                        System.out.print("=");
                    }
                    System.out.print("/");
                    for (int j = 1; j <= kl; j++) {//membuat spasi setelah mencetak batik diamond
                        System.out.print(" ");
                    }
                }
                System.out.print("|");
                System.out.println("");
            }
            for (int kl = 0; kl < uk; kl++) {//membuat kolom batik diamond bawah 
                System.out.print("|");
                for (int i = 0; i < uk; i++) {
                    for (int j = uk - 1; j > kl; j--) {//membuat spasi sebelum mencetak batik diamond
                        System.out.print(" ");
                    }
                    System.out.print("/");
                    for (int k = 0; k < kl; k++) {//mencetak batik diamond segitiga kiri
                        System.out.print("=");
                    }
                    for (int j = 0; j < kl; j++) {
                        System.out.print("=");//mencetak batik diamon segitiga kanan
                    }
                    System.out.print("\\");
                    for (int j = uk - 1; j > kl; j--) {//membuat spasi setelah mencetak batik diamond
                        System.out.print(" ");
                    }
                }
                System.out.print("|");
                System.out.println(""); 
            }
            System.out.print("");
        }
    }
}
