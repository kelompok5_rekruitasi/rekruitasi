/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectliburan;

/**
 *
 * @author Alwan
 */
public class CaseDecryptMessage {

    public static void main(String[] args) {
        String kdbinary = "";
        String s = null;
        String kdascii="";
        int p=0;
        String kdrahasia = "-.-..-.-..-.-..--.-..-.--.-.--.--....-..-......--.-.--.--....-.--.--.-.---."
                + "-.-..-......--...-..--..-.-.--.--...---.-.-.--.--.-..-......--.--.-.--..-.-.---..-..--...."
                + "-.--.-..-.--.-.....-......--.-.--.--..-.-.---..--.---.-.-.--.-.--.---..--.--..-.-.---..--.--"
                + "....-.--.---...-.--....-......--.-.-..--....-.--.---..--..---.--....-.--.---...-......---.....--."
                + ".-.-.---..-..--.---..--....-.--.-.....-......--...-..--..-.-.---..-..--.-....--..-.-.--.---..---.-...--"
                + ".-..-..-......---.-.-.--.---..---.-...---.-.-.--.-.--..-......---.-...--..-.-.---..-..---.-.-.---..--..-.."
                + "....--.--.-.--..-.-.--.---..--...--.--.----.--...-..--....-..-....-..--.--.";

        //Proses Mengubah Kode Rahasia Menjadi Biner
        for (int i = 0; i < kdrahasia.length(); i++) {
            String sk = kdrahasia.substring(i, i + 1);
            if (sk.equalsIgnoreCase(".")) {
                kdbinary += "0";
            } else {
                kdbinary += "1";
            }
        }

        if (s == null) {//kondisi jika sufix belum ada 
            s = kdbinary.substring(kdbinary.length() - 8, kdbinary.length());//mengambil 8 bit biner untuk mencari suffix
            s = KonverBinaryKeASCIIKeHuruf(s);//membaca 8 bit biner suffix  mencari ASCII(banyak prefix)
        }

        p = Integer.parseInt(s);//hasil desimal String di parsing menjadi Integer

        String Binary = kdbinary.substring(p, kdbinary.length()-8);//membaca index dari mulai nilai sufix

        String biner = null;
        int pr = 0;
        for (int i = 0; i < Binary.length() / 8; i++) {//membagi menjadi 8 bit biner 
            biner = Binary.substring(pr, pr + 8);//mengambil 8 bit biner 

            kdascii = KonverBinaryKeASCIIKeHuruf(biner);
            pr = pr + 8;
            System.out.print(kdascii);
        }
        System.out.println("");
    }

    //Proses Konver Biner Ke ASCII
    static String KonverBinaryKeASCIIKeHuruf(String binary) {
        StringBuilder sb = new StringBuilder();
        int[] p = {1, 2, 4, 8, 16, 32, 64, 128};
        for (int i = 0; i < binary.length(); i += 9) {
            int index = 0;
            int jml = 0;
            for (int j = 7; j >= 0; j--) {
                if (binary.charAt(j + i) == '1') {
                    jml += p[index];
                }
                index++;
            }
            sb.append(Character.toChars(jml));
        }
        return sb.toString();
    }
}
