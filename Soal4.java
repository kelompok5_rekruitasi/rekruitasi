package soal4;

import java.util.Scanner;
import static java.lang.Math.sqrt;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Soal4 {

    public static void main(String[] args) {
       Scanner scan = new Scanner(System.in);
        int na = 0;
        int nb = 0;
        int nc = 0;
      
        int nh = 0;
        System.out.println("Masukan Akar Persamaan Kuadrat= ");
        String akar = scan.nextLine();
       

        if (!akar.startsWith("+") && !akar.startsWith("-")) {
            akar = "+" + akar;
        }

        String exp = "^((.*)x\\^2)?((.*)x)?([\\+\\s\\-\\d]*)?=([\\+\\s\\-\\d]*)?";//membuat syarat  pola string agar sesuai dengan akar persamaan kuadrat
        //                a x  ^2     b x           c        =   d     
        Pattern p = Pattern.compile(exp);
        Matcher m = p.matcher(akar);

        while (m.find()) {// untuk mencari nilai a b c d
            na = cariNilai(m.group(2));
            nb = cariNilai(m.group(4));
            nc = cariNilai(m.group(5));
            nh = cariNilai(m.group(6));
        }
        System.out.println("Persamaan Kuadrat : ");
        
       if (nh != 0) {
            int h = nh * -1;
            nh=0;
            if (nh < 0) {
                nc -= h;
                nh=0;
            } else {
                nc += h;
                nh=0;
            }
        }
        if (na == 1) {
            System.out.print("x^2");
        } else if (na > 0) {
            System.out.print(na + "x^2");
        }

        if (nb <= -2) {
            System.out.print((nb) + "x");
        } else if (nb == -1) {
            System.out.print("-" + "x");
        } else if (nb == 1) {
            System.out.print("+" + "x");
        } else if (nb > 0) {
            System.out.print("+" + nb + "x");
        }

        if (nc < 0) {
            System.out.print("-" + (-nc));
        } else if (nc > 0) {
            System.out.print("+" + nc);

        }
        if (nh < 0) {
            System.out.print("=" + (-nh));
        } else if (nh > 0) {
            System.out.println("=" + (nh));
        } else if (nh == 0) {
            System.out.println("=" + nh);
        }

        int det = nb * nb - (4 * na * nc);
        int x1 = (int) (-nb + sqrt(det)) / (2 * na);
        int x2 = (int) (-nb - sqrt(det)) / (2 * na);

        if (det < 0) {
            System.out.println("Tidak dapat difaktorkan");
        }

        if (x1 > 0) {
            System.out.print("(x" + (-x1) + ")");
        } else if (x1 < 0) {
            System.out.print("(x+" + (-x1) + ")");
        }
        if (x2 > 0) {
            System.out.println("(x" + (-x2) + ")");
        } else if (x2 < 0) {
            System.out.println("(x+" + (-x2) + ")");

        }
        if (det == 0) {
            System.out.println("X = " + x1);
        } else {
            System.out.println("X1 = " + x1 + " atau " + "X2 = " + x2);
        }

    }
     private static int cariNilai(String data) {
        if (data == null) {//jika data null makan hasil nya 0
            return 0;
        } else {

            if (data.equals("+")) {// jika data + maka haislnya 1  
                return 1;
            } else if (data.equals("-")) {//jika data - maka hasilnya -1
                return -1;
            } else {// jika ada nilai akan di parsing atau di ubah menjadi float
                try {
                    int num = (int) Float.parseFloat(data);
                    return num;
                } catch (NumberFormatException ex) {//kondisi jika terjadi galat
                    return 0;
                }
            }
        }
    }
}
